from setuptools import setup


setup(
    name="simulador",
    version="0.0.1",
    description="",
    url="",
    author="Brenno Flávio",
    author_email="brenno.flavio@fluxoresultados.com.br",
    keywords="simulador",
    packages=["simulador"],
    zip_safe=False,
)
