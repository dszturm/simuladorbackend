# /bin/sh

rm -rf /srv/logs
mkdir /srv/logs/
touch /srv/logs/gunicorn.log
touch /srv/logs/access.log
tail -n 0 -f /srv/logs/*.log &

#export FLASK_RUN_PORT=5000
#export FLASK_APP=tamandua.app
#export FLASK_ENV=development

echo Starting Gunicorn
#flask run
 exec gunicorn simulador.wsgi:app \
     --bind 0.0.0.0:5000 \
     --chdir /app/simulador \
     --workers 3 \
     --log-level=debug \
     --log-file=/srv/logs/gunicorn.log \
     --access-logfile=/srv/logs/access.log \
     --timeout 110 \
     --limit-request-line 0
