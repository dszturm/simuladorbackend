from flask import Flask, Blueprint, request
from flask_cors import CORS
from simulador.api import api

from healthcheck import HealthCheck, EnvironmentDump

from simulador.api.health_insurances import ns as health_insurances_ns
from simulador.api.products import ns as products_ns
from simulador.api.orders import ns as orders_ns

# from simulador.api.coverages import ns as coverages_ns
from simulador.api.accommodations import ns as accommodations_ns
from simulador.api.locations import ns as locations_ns
from flask import render_template
import itertools
import json
from base64 import urlsafe_b64decode


def grouper(n, iterable):
    args = [iter(iterable)] * n
    return list([e for e in t] for t in itertools.zip_longest(*args))


def create_app():
    app = Flask(__name__)

    health = HealthCheck()
    envdump = EnvironmentDump()

    CORS(app)

    blueprint = Blueprint("api", __name__, static_url_path="")
    api.init_app(blueprint)
    # api.add_namespace(simulate_ns, "/v1/simulate")
    api.add_namespace(products_ns, "/v1/product")
    api.add_namespace(health_insurances_ns, "/v1/health-company")
    api.add_namespace(orders_ns, "/v1/order")
    # api.add_namespace(coverages_ns, "/v1/coverage")
    api.add_namespace(accommodations_ns, "/v1/accommodation")
    api.add_namespace(locations_ns, "/v1/location")

    app.register_blueprint(blueprint)
    app.add_url_rule("/health-check", "healthcheck", view_func=lambda: health.run())
    app.add_url_rule("/environment", "environment", view_func=lambda: envdump.run())

    @app.route("/simulacoes")
    def render_document():
        insurance_data = json.loads(
            (urlsafe_b64decode(request.args.get("data")).decode())
        )
        company = request.args.get("company")

        for row in insurance_data:
            if row.get("health_insurance").lower() == "bradesco":
                row["iof"] = round((float(row.get("amount", 0)) * 0.0238))
            else:
                row["iof"] = 0

        insurance_companies = list(
            set([x.get("health_insurance") for x in insurance_data])
        )

        hospitals = []
        laboratories = []
        for row in insurance_data:
            hospitals.extend(row.get("hospitals", []))
            laboratories.extend(row.get("laboratories", []))

        hospitals = list(set(hospitals))
        laboratories = list(set(laboratories))

        insurante_data_template = {}

        for insurance in insurance_companies:
            insurante_data_template[insurance] = {}
            insurance_plans = [
                x for x in insurance_data if x.get("health_insurance", "") == insurance
            ]
            insurante_data_template[insurance]["table_data"] = grouper(
                3, insurance_plans
            )

        return render_template(
            "index.html",
            company=company,
            insurance_companies=insurance_companies,
            insurante_data_template=insurante_data_template,
            hospitals=hospitals,
            laboratories=laboratories,
        )

    return app
