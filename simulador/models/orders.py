from sqlalchemy import (
    Column,
    BigInteger,
    Text,
    Binary,
    JSON,
    ForeignKey,
    DateTime,
    Boolean,
    ARRAY,
)
from .base import Base


class Order(Base):
    __tablename__ = "orders"

    id = Column(BigInteger, primary_key=True)
    plan = Column(BigInteger)
    company = Column(Text)
    company_contact = Column(Text)
    telephone = Column(Text)
    coverage = Column(Text)
    coparticipation = Column(Boolean)
    compulsory = Column(Boolean)
    state = Column(Text)
    city = Column(Text)
    health_insurances = Column(ARRAY(BigInteger))
    cnpj_type = Column(BigInteger)
    age_0_18holder = Column(BigInteger)
    age_0_18dependent = Column(BigInteger)
    age_19_23holder = Column(BigInteger)
    age_19_23dependent = Column(BigInteger)
    age_24_28holder = Column(BigInteger)
    age_24_28dependent = Column(BigInteger)
    age_29_33holder = Column(BigInteger)
    age_29_33dependent = Column(BigInteger)
    age_34_38holder = Column(BigInteger)
    age_34_38dependent = Column(BigInteger)
    age_39_43holder = Column(BigInteger)
    age_39_43dependent = Column(BigInteger)
    age_44_48holder = Column(BigInteger)
    age_44_48dependent = Column(BigInteger)
    age_49_53holder = Column(BigInteger)
    age_49_53dependent = Column(BigInteger)
    age_54_58holder = Column(BigInteger)
    age_54_58dependent = Column(BigInteger)
    age_59_moreholder = Column(BigInteger)
    age_59_moredependent = Column(BigInteger)
