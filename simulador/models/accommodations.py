from sqlalchemy import Column, BigInteger, Text
from .base import Base


class Accommodation(Base):
    __tablename__ = "accommodations"

    id = Column(BigInteger, primary_key=True)
    name = Column(Text)
