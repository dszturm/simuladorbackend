from sqlalchemy import (
    Column,
    BigInteger,
    Text,
    Binary,
    Float,
    ForeignKey,
    DateTime,
    Boolean,
    JSON,
)
from .base import Base


class ConfirmedOrders(Base):
    __tablename__ = "confirmed_orders"

    id = Column(BigInteger, primary_key=True)
    order_id = Column(BigInteger, ForeignKey("orders.id"), nullable=False)
    coverage_id = Column(BigInteger, ForeignKey("coverages.id"), nullable=False)
    product_id = Column(BigInteger, ForeignKey("products.id"), nullable=False)
    accommodation_id = Column(
        BigInteger, ForeignKey("accommodations.id"), nullable=False
    )
    coparticipation = Column(Boolean, nullable=False)
    compulsory = Column(Boolean, nullable=False)
    quantity_lives = Column(JSON, nullable=False)
    total_value = Column(Float, nullable=False)
