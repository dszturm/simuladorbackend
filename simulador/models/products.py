from sqlalchemy import (
    Column,
    BigInteger,
    Text,
    JSON,
    ForeignKey,
    Boolean,
    Float,
    UniqueConstraint,
    Index,
    ForeignKey,
)
from .base import Base
from sqlalchemy.dialects.postgresql import JSONB


class Products(Base):
    __tablename__ = "products"

    id = Column(BigInteger, primary_key=True)
    plan = Column(BigInteger)
    ans = Column(Text, nullable=False)
    name = Column(Text, nullable=False)
    health_insurance_id = Column(
        BigInteger, ForeignKey("health_insurances.id"), nullable=False
    )
    accommodation = Column(Text, nullable=False)
    coverage = Column(Text, nullable=False)
    compulsory = Column(Boolean, nullable=False)
    coparticipation = Column(Boolean, nullable=False)
    state = Column(Text, nullable=False)
    city = Column(Text)
    quantity_lives = Column(Text, nullable=False)
    age_range = Column(Text)
    cnpj_type = Column(Text)
    amount = Column(Float)
    hospitals = Column(JSONB)
    laboratories = Column(JSONB)
    __table_args__ = (
        UniqueConstraint(
            "name",
            "health_insurance_id",
            "accommodation",
            "compulsory",
            "coparticipation",
            "coverage",
            "state",
            "city",
            "ans",
            "quantity_lives",
            "age_range",
            "plan",
            "cnpj_type",
            name="uix_products_all",
        ),
        Index(
            "uix_city_null",
            "name",
            "ans",
            "health_insurance_id",
            "accommodation",
            "coverage",
            "compulsory",
            "coparticipation",
            "state",
            "quantity_lives",
            "age_range",
            "plan",
            "cnpj_type",
            unique=True,
            postgresql_where=city.is_(None),
        ),
        Index(
            "uix_cnpj_type_null",
            "name",
            "ans",
            "health_insurance_id",
            "accommodation",
            "coverage",
            "compulsory",
            "coparticipation",
            "state",
            "quantity_lives",
            "age_range",
            "plan",
            "city",
            unique=True,
            postgresql_where=cnpj_type.is_(None),
        ),
    )
