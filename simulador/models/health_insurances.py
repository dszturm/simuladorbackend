from sqlalchemy import Column, BigInteger, Text
from .base import Base


class HealthInsurances(Base):
    __tablename__ = "health_insurances"

    id = Column(BigInteger, primary_key=True)
    name = Column(Text)
