from sqlalchemy import MetaData
from sqlalchemy.ext.declarative import declarative_base
import os

metadata = (
    None
    if not os.getenv("SQL_ALCHEMY_SCHEMA") or os.getenv("SQL_ALCHEMY_SCHEMA").isspace()
    else MetaData(schema=os.getenv("SQL_ALCHEMY_SCHEMA"))
)

Base = declarative_base(metadata=metadata)
