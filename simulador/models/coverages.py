from sqlalchemy import Column, BigInteger, Text, BINARY, JSON, ForeignKey
from .base import Base


class Coverage(Base):
    __tablename__ = "coverages"

    id = Column(BigInteger, primary_key=True)
    name = Column(Text)
