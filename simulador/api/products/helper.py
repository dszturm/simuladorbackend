from simulador.utils.db import provide_session
from simulador.models.products import Products
from simulador.models.health_insurances import HealthInsurances
from sqlalchemy.exc import IntegrityError

age_fields = [
    "age_range",
    "age_0_18",
    "age_19_23",
    "age_24_28",
    "age_29_33",
    "age_34_38",
    "age_39_43",
    "age_44_48",
    "age_49_53",
    "age_54_58",
    "age_59_more",
]

quantity_lives_range = ["2 - 29", "30 - 99", "100+"]


def check_errors_quantity_lives_range_sent(new_product):
    age_quantity_lives_sent = {}

    for v in quantity_lives_range:
        age_quantity_lives_sent[v] = 0

    for item_in_range in new_product["data"]:
        for quantity_lives in quantity_lives_range:
            if item_in_range["age_range"] == quantity_lives:
                age_quantity_lives_sent[quantity_lives] = (
                    age_quantity_lives_sent.get(quantity_lives, 0) + 1
                )

    for item in quantity_lives_range:
        if age_quantity_lives_sent[item] > 1:
            return True

    return False


@provide_session
def process_request(payload, session=None):
    # payload = {"name": "Bradesco"}

    new_product = {}
    new_product["name"] = payload.get("name")
    new_product["ans"] = payload.get("ans")
    new_product["health_insurance_id"] = payload.get("health_insurance_id")
    new_product["coverage"] = payload.get("coverage")
    new_product["accommodation"] = payload.get("accommodation")
    new_product["compulsory"] = payload.get("compulsory")
    new_product["coparticipation"] = payload.get("coparticipation")
    new_product["state"] = payload.get("state")
    new_product["city"] = payload.get("city")
    new_product["cnpj_type"] = payload.get("cnpj_type")
    new_product["plan"] = payload.get("plan")
    new_product["data"] = payload.get("data")
    new_product["hospitals"] = [x for x in payload.get("hospitals").split("\n") if x]
    new_product["laboratories"] = [
        x for x in payload.get("laboratories").split("\n") if x
    ]

    # if check_errors_quantity_lives_range_sent(new_product):
    #     return {
    #         "error": "Campo 'data' deve ter três 'age_range' diferentees. "
    #         + str(quantity_lives_range)
    #     }

    if (
        not session.query(HealthInsurances)
        .filter(HealthInsurances.id == new_product.get("health_insurance_id"))
        .all()
    ):
        return {"error": "ID de Operadora inválido."}

    # for key in new_product:
    #     if type(new_product[key]) == str:
    #         new_product[key] = str(new_product[key]).strip().lower()

    products = [new_product]
    if None is new_product.get("coparticipation"):
        products = add_null_fields([True, False], "coparticipation", products)

    if None is new_product.get("compulsory"):
        products = add_null_fields([True, False], "compulsory", products)

    if None is new_product.get("coverage"):
        products = add_null_fields(["regional", "nacional"], "coverage", products)

    # if not new_product.get("quantity_lives"):
    #     products = add_null_fields(
    #         ["2-29", "30-99", "100-more"], "quantity_lives", products
    #     )

    if not new_product.get("accommodation"):
        products = add_null_fields(
            ["quarto", "apartamento", "enfermaria"], "accommodation", products
        )
    try:
        products_list = []
        for product in products:
            data = new_product.get("data")
            for item in data:
                new = {}
                new["quantity_lives"] = item.get("age_range")
                prices = item.copy()
                del prices["age_range"]
                for linha in prices:
                    # if item.get(linha) is None:
                    #     continue
                    product_copy = product.copy()
                    del product_copy["data"]
                    new = {**product_copy, **new}
                    new["age_range"] = linha
                    new["amount"] = item.get(linha, 0)
                    p = Products(**new)
                    products_list.append(p)
                    session.add(p)
                del new

        id_list = []

        session.commit()
        for product in products_list:
            id_list.append(product.id)
    except IntegrityError:
        session.rollback()
        return {"error": "Conjunto de informações já cadastrado."}

    return {"id": id_list}


def add_null_fields(enum_of_field_name, field_name, initial_list=[]):
    final_list = []
    for item in initial_list:
        for enum in enum_of_field_name:
            final_list.append({**item, field_name: enum})
    return final_list


@provide_session
def get_all(session=None):
    items = []
    query = session.query(Products, HealthInsurances.name).join(HealthInsurances)
    for item, k in query.all():
        new_item = item.__dict__
        new_item["health_insurance"] = k
        del new_item["_sa_instance_state"]
        del new_item["health_insurance_id"]
        age = new_item.get("age_range")
        if age:
            age = age.split("_")
            age = f"{age[1]} - {age[2]}"
            new_item["age_range"] = age
        new_item["compulsory"] = "Sim" if new_item.get("compulsory") else "Não"
        new_item["coparticipation"] = "Sim" if new_item.get("compulsory") else "Não"
        new_item["coverage"] = str.capitalize(new_item.get("coverage"))
        new_item["accommodation"] = str.capitalize(new_item.get("accommodation"))

        items.append(new_item)

    return items


@provide_session
def get_item_by_id(id, session=None):
    items = []
    for item in session.query(Products).filter_by(id=id).all():
        new_item = item.__dict__
        del new_item["_sa_instance_state"]
        items.append(new_item)

    return items


@provide_session
def delete_item_by_id(id: int, session=None):
    try:
        product = session.query(Products).filter(Products.id == id).first()
        if not product:
            return {"message": "ID de Produto não encontrado"}, 400
        name = product.name
        session.query(Products).filter(Products.id == id).delete()
        session.commit()
        return (
            {"message": "Produto de 'id: {}' and 'nome: {}' deletado".format(id, name)},
            200,
        )
    except IntegrityError:
        return {"error": "Produto utilizado em alguma Ordem. Produto não foi deletado."}
    except Exception as identifier:
        return {"error": str(identifier.__class__)}, 400
