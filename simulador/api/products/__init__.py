from flask_restplus import Resource, reqparse
from flask import request
from simulador.api import api
from .helper import process_request, get_all
from .helper import age_fields, quantity_lives_range, delete_item_by_id


ns = api.namespace("v1/product", description="Product health plan")

# parser_auth = reqparse.RequestParser()
# parser_auth.add_argument("Authorization", location="headers", required=True)
# parser_auth.add_argument("teste", type=int, required=False)
# parser_auth.add_argument("teste2", action="split") # splita uma string pela virgula, semelhante a str.split(",")
parser_info = reqparse.RequestParser()
parser_info.add_argument("id", type=int, required=True)


product_body = {
    "type:": "object",
    "required": [
        "name",
        "health_insurance_id",
        "state",
        "data",
        "ans",
        "plan",
        "coverage",
        "accommodation",
        "compulsory",
        "coparticipation",
    ],
    "properties": {
        "laboratories": {"type": "string"},
        "hospitals": {"type": "string"},
        "name": {"type": "string", "minLength": 1},
        "ans": {"type": "string", "minLength": 1},
        "plan": {"type": "integer"},
        "health_insurance_id": {"type": "integer"},
        "coverage": {"type": "string", "enum": ["regional", "nacional"]},
        "accommodation": {
            "type": "string",
            "enum": ["quarto", "apartamento", "enfermaria"],
        },
        "cnpj_type": {
            "type": "string",
            # "enum": ["MEI", "EI", "EPP", "EIRELI", "LTDA", "SA"],
        },
        "compulsory": {"type": "boolean"},
        "coparticipation": {"type": "boolean"},
        "state": {"type": "string", "maxLength": 2},
        "city": {"type": "string", "minLength": 3},
        "data": {
            "type": "array",
            "minItems": 1,
            "maxItems": 3,
            "items": {
                "type": "object",
                "required": age_fields,
                "properties": {
                    "age_range": {
                        "type": "string",
                        "enum": quantity_lives_range,
                        "uniqueItems": True,
                    },
                    "age_0_18": {"type": "string"},
                    "age_19_23": {"type": "string"},
                    "age_24_28": {"type": "string"},
                    "age_29_33": {"type": "string"},
                    "age_34_38": {"type": "string"},
                    "age_39_43": {"type": "string"},
                    "age_44_48": {"type": "string"},
                    "age_49_53": {"type": "string"},
                    "age_54_58": {"type": "string"},
                    "age_59_more": {"type": "string"},
                },
            },
        },
    },
}


post_product = api.schema_model("POST Products V1", product_body)


@ns.route("")
class ProductRoute(Resource):
    @ns.expect(None, post_product, validate=True)
    @api.response(200, "Success")
    def post(self):
        response = process_request(dict(request.json))

        if response.get("error"):
            return response, 400

        # response = {"test": "testado"}
        return response, 200

    @api.response(200, "Success")
    def get(self):
        return get_all()


@ns.route("/<id>")
@ns.param("id", "Product's identification number")
class ProductIdRoute(Resource):
    @ns.expect(parser_info, validate=True)
    @api.response(200, "Success", post_product)
    def delete(self, id):
        return delete_item_by_id(int(id))
