from flask_restplus import Resource, reqparse
from flask import request, send_file
from simulador.api import api
from .helper import (
    process_request,
    calculate_sheets_information,
)
from fpdf import FPDF
from tempfile import NamedTemporaryFile
from simulador.api.orders.helper_patch import patch_request

ns = api.namespace("v1/order", description="Order")

parser_info = reqparse.RequestParser()
parser_info.add_argument("id", type=int, required=True)

range_age = {
    "type": "object",
    "properties": {
        "age_0_18": {"type": "integer"},
        "age_19_23": {"type": "integer"},
        "age_24_28": {"type": "integer"},
        "age_29_33": {"type": "integer"},
        "age_34_48": {"type": "integer"},
        "age_39_43": {"type": "integer"},
        "age_44_48": {"type": "integer"},
        "age_49_53": {"type": "integer"},
        "age_54_58": {"type": "integer"},
        "age_59_more": {"type": "integer"},
    },
}


orders_body = {
    "type:": "object",
    "required": [
        "company",
        "company_contact",
        "telephone",
        "state",
        "health_insurance",
    ],
    "properties": {
        "plan": {"type": "integer"},
        "company": {"type": "string"},
        "company_contact": {"type": "string"},
        "telephone": {"type": "string"},
        "coparticipation": {"type": "boolean"},
        "coverage": {"type": "string"},
        "compulsory": {"type": "boolean"},
        "health_insurance": {
            "type": "array",
            "items": {"type": "string"},
            "uniqueItems": True,
            "minItems": 1,
        },
        "state": {"type": "string"},
        "city": {"type": "string"},
        "cnpj_type": {"type": "string"},
        "age_0_18holder": {"type": "number"},
        "age_0_18dependent": {"type": "number"},
        "age_19_23holder": {"type": "number"},
        "age_19_23dependent": {"type": "number"},
        "age_24_28holder": {"type": "number"},
        "age_24_28dependent": {"type": "number"},
        "age_29_33holder": {"type": "number"},
        "age_29_33dependent": {"type": "number"},
        "age_34_38holder": {"type": "number"},
        "age_34_38dependent": {"type": "number"},
        "age_39_43holder": {"type": "number"},
        "age_39_43dependent": {"type": "number"},
        "age_44_48holder": {"type": "number"},
        "age_44_48dependent": {"type": "number"},
        "age_49_53holder": {"type": "number"},
        "age_49_53dependent": {"type": "number"},
        "age_54_58holder": {"type": "number"},
        "age_54_58dependent": {"type": "number"},
        "age_59_moreholder": {"type": "number"},
        "age_59_moredependent": {"type": "number"},
    },
}

orders_pdf_body = {
    "type": "array",
    "minItems": 1,
    "items": {
        "properties": {
            "name": {"type": "string"},
            "health_insurance": {"type": "string"},
            "accommodation": {"type": "string"},
            "compulsory": {"type": "string"},
            "coparticipation": {"type": "string"},
            "coverage": {"type": "string"},
            "age_0_18": {"type": "number"},
            "age_19_23": {"type": "number"},
            "age_24_28": {"type": "number"},
            "age_29_33": {"type": "number"},
            "age_34_38": {"type": "number"},
            "age_39_43": {"type": "number"},
            "age_44_48": {"type": "number"},
            "age_49_53": {"type": "number"},
            "age_54_58": {"type": "number"},
            "age_59_more": {"type": "number"},
            "company": {"type": "string"},
            "hospitals": {"type": "array", "items": {"type": "string"}},
            "laboratories": {"type": "array", "items": {"type": "string"}},
        },
    },
}

order_upload_response = {
    "type": "array",
    "items": {
        "properties": {
            "name": {"type": "string"},
            "health_insurance": {"type": "string"},
            "accommodation": {"type": "string"},
            "compulsory": {"type": "string"},
            "coparticipation": {"type": "string"},
            "coverage": {"type": "string"},
            "amount": {"type": "number"},
            "age_0_18": {"type": "number"},
            "age_19_23": {"type": "number"},
            "age_24_28": {"type": "number"},
            "age_29_33": {"type": "number"},
            "age_34_38": {"type": "number"},
            "age_39_43": {"type": "number"},
            "age_44_48": {"type": "number"},
            "age_49_53": {"type": "number"},
            "age_54_58": {"type": "number"},
            "age_59_more": {"type": "number"},
            "spg": {"type": "string"},
        }
    },
}

post_orders_schema = api.schema_model("POST Orders V1", orders_body)
patch_orders_schema = api.schema_model("PATCH Orders V1", orders_pdf_body)
post_orders_upload_schema = api.schema_model(
    "POST Orders Product V1", order_upload_response
)


@ns.route("")
class Order(Resource):
    @ns.expect(None, post_orders_schema, validate=True)
    @api.response(200, "Success")
    def post(self):
        order_item = dict(request.json).copy()
        response = process_request(order_item)

        return response, 200

    @ns.expect(None, patch_orders_schema, validate=True)
    @api.response(200, "Success")
    def patch(self):
        return patch_request(list(request.json))


@ns.route("/upload")
# @ns.param("id", "Order's identification number")
class OrderUploadRoute(Resource):
    # @ns.expect(post_orders_upload_schema, validate=True)
    @api.response(200, "Success", post_orders_upload_schema)
    def post(self):
        return calculate_sheets_information(request.get_data())

    @api.response(200, "Success")
    def get(self):
        # return listdir("./simulador/api/orders/excelsimulador.xlsx")
        return send_file("api/orders/excelsimulador.xlsx", as_attachment=True)
