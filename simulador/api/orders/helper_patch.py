from simulador.utils.db import provide_session
from simulador.models.orders import Order
from simulador.models.products import Products
from simulador.models.health_insurances import HealthInsurances
import pandas
from tempfile import NamedTemporaryFile
import base64
import json
import os
from flask import send_file
import imgkit
from PIL import Image


def patch_request(data):
    insurance_data = data.copy()

    # insurance_data = [
    #     {
    #         "name": "TOP NACIONAL",
    #         "health_insurance": "Bradesco",
    #         "accommodation": "enfermaria",
    #         "compulsory": "Não",
    #         "coparticipation": "Não",
    #         "coverage": "nacional",
    #         "amount": 150,
    #         "age_0_18": 0,
    #         "age_19_23": 0,
    #         "age_24_28": 150,
    #         "age_29_33": 0,
    #         "age_34_38": 0,
    #         "age_39_43": 0,
    #         "age_44_48": 0,
    #         "age_49_53": 0,
    #         "age_54_58": 0,
    #         "age_59_more": 0,
    #         "spg": "2 - 29 vidas",
    #         "tableData": {"id": 0, "checked": True},
    #         "hospitals": ["Hospital a"],
    #         "laboratories": ["Laboratório a", "Laboratório b"],
    #     },
    #     {
    #         "name": "TOP NACIONAL ENFERMARIA",
    #         "health_insurance": "Bradesco",
    #         "accommodation": "enfermaria",
    #         "compulsory": "Não",
    #         "coparticipation": "Não",
    #         "coverage": "nacional",
    #         "amount": 150,
    #         "age_0_18": 0,
    #         "age_19_23": 0,
    #         "age_24_28": 150,
    #         "age_29_33": 0,
    #         "age_34_38": 0,
    #         "age_39_43": 0,
    #         "age_44_48": 0,
    #         "age_49_53": 0,
    #         "age_54_58": 0,
    #         "age_59_more": 0,
    #         "spg": "2 - 29 vidas",
    #         "tableData": {"id": 1, "checked": True},
    #         "hospitals": ["Hospital a", "Hospital b"],
    #         "laboratories": ["Laboratório a", "Laboratório b"],
    #     },
    #     {
    #         "name": "TOP NACIONAL ENFERMARIA2",
    #         "health_insurance": "Bradesco",
    #         "accommodation": "enfermaria",
    #         "compulsory": "Não",
    #         "coparticipation": "Não",
    #         "coverage": "nacional",
    #         "amount": 150,
    #         "age_0_18": 0,
    #         "age_19_23": 0,
    #         "age_24_28": 150,
    #         "age_29_33": 0,
    #         "age_34_38": 0,
    #         "age_39_43": 0,
    #         "age_44_48": 0,
    #         "age_49_53": 0,
    #         "age_54_58": 0,
    #         "age_59_more": 0,
    #         "spg": "2 - 29 vidas",
    #         "tableData": {"id": 1, "checked": True},
    #         "hospitals": ["Hospital a", "Hospital b"],
    #         "laboratories": ["Laboratório a", "Laboratório b"],
    #     },
    #     {
    #         "name": "TOP NACIONAL ENFERMARIA3",
    #         "health_insurance": "Bradesco",
    #         "accommodation": "enfermaria",
    #         "compulsory": "Não",
    #         "coparticipation": "Não",
    #         "coverage": "nacional",
    #         "amount": 150,
    #         "age_0_18": 0,
    #         "age_19_23": 0,
    #         "age_24_28": 150,
    #         "age_29_33": 0,
    #         "age_34_38": 0,
    #         "age_39_43": 0,
    #         "age_44_48": 0,
    #         "age_49_53": 0,
    #         "age_54_58": 0,
    #         "age_59_more": 0,
    #         "spg": "2 - 29 vidas",
    #         "tableData": {"id": 1, "checked": True},
    #         "hospitals": ["Hospital a", "Hospital b"],
    #         "laboratories": ["Laboratório a", "Laboratório b"],
    #     },
    #     {
    #         "name": "TOP NACIONAL ENFERMARIA3",
    #         "health_insurance": "Amil",
    #         "accommodation": "enfermaria",
    #         "compulsory": "Não",
    #         "coparticipation": "Não",
    #         "coverage": "nacional",
    #         "amount": 150,
    #         "age_0_18": 0,
    #         "age_19_23": 0,
    #         "age_24_28": 150,
    #         "age_29_33": 0,
    #         "age_34_38": 0,
    #         "age_39_43": 0,
    #         "age_44_48": 0,
    #         "age_49_53": 0,
    #         "age_54_58": 0,
    #         "age_59_more": 0,
    #         "spg": "2 - 29 vidas",
    #         "tableData": {"id": 1, "checked": True},
    #         "hospitals": ["Hospital a", "Hospital b"],
    #         "laboratories": ["Laboratório a"],
    #     },
    # ]
    company = insurance_data[0].get("company", "Afinidade")

    base64str = base64.urlsafe_b64encode(json.dumps(insurance_data).encode()).decode()

    url = f"{os.getenv('BASE_URL')}/simulacoes?data={base64str}&company={company}"

    with NamedTemporaryFile(
        prefix="simulacao", suffix=".jpg"
    ) as jpg_file, NamedTemporaryFile(prefix="simulacao", suffix=".pdf") as pdf_file:
        imgkit.from_url(url, jpg_file.name)

        image1 = Image.open(jpg_file.name)
        im1 = image1.convert("RGB")
        im1.save(pdf_file.name)

        return send_file(pdf_file.name, as_attachment=True)
