from simulador.utils.db import provide_session
from simulador.models.orders import Order
from simulador.models.products import Products
from simulador.models.health_insurances import HealthInsurances
import pandas
from tempfile import NamedTemporaryFile

BRADESCO_ID = 40


@provide_session
def process_request(payload, session=None):
    result = []
    if BRADESCO_ID in payload.get("health_insurance"):
        result = post_request_bradesco(payload)
        ids = payload["health_insurance"]
        ids.remove(BRADESCO_ID)
        payload["health_insurance"] = ids
    return result + post_request(payload)


def consolidate_info(query, final_dict):
    query = [{**x.__dict__, "health_insurance": k} for x, k in query]

    for product in query:
        age_range = product.get("age_range")
        product["amount"] = product.get("amount", 0.0) * final_dict.get(age_range, 0.0)
        # return [age_range]
        product[age_range] = product.get("amount", 0.0)
        del product["_sa_instance_state"]
        # return [product]

    # return (query)
    for item in query:
        # del item["_sa_instance_state"]
        del item["age_range"]

    for item in query:
        item["hospitals"] = tuple(item["hospitals"] if item["hospitals"] else [])
        item["laboratories"] = tuple(item["laboratories"] if item["laboratories"] else [])

    # return (query)

    data_frame = pandas.DataFrame.from_dict(query)
    group_by_columns = [
        "name",
        "health_insurance",
        "accommodation",
        "compulsory",
        "coparticipation",
        "coverage",
        "state",
        "city",
        "ans",
        "quantity_lives",
        # "age_range",
        "plan",
        "cnpj_type",
        # "amount",
        "hospitals",
        "laboratories",
    ]

    data_frame = pandas.DataFrame(data_frame.fillna(value=False))
    data_frame = (
        data_frame.groupby(group_by_columns)
        .agg(
            {
                "amount": "sum",
                "age_0_18": "sum",
                "age_19_23": "sum",
                "age_24_28": "sum",
                "age_29_33": "sum",
                "age_34_38": "sum",
                "age_39_43": "sum",
                "age_44_48": "sum",
                "age_49_53": "sum",
                "age_54_58": "sum",
                "age_59_more": "sum",
            }
        )
        .reset_index()
        .to_dict(orient="records")
    )
    return data_frame


@provide_session
def post_request(payload, session=None):
    quantity_keys = [
        "age_0_18holder",
        "age_0_18dependent",
        "age_19_23holder",
        "age_19_23dependent",
        "age_24_28holder",
        "age_24_28dependent",
        "age_29_33holder",
        "age_29_33dependent",
        "age_34_38holder",
        "age_34_38dependent",
        "age_39_43holder",
        "age_39_43dependent",
        "age_44_48holder",
        "age_44_48dependent",
        "age_49_53holder",
        "age_49_53dependent",
        "age_54_58holder",
        "age_54_58dependent",
        "age_59_moreholder",
        "age_59_moredependent",
    ]

    quantity_lives = sum([int(v) for k, v in payload.items() if k in quantity_keys])

    if quantity_lives < 2:
        return []
    elif quantity_lives < 30:
        quantity_lives_str = "2 - 29"
    elif quantity_lives < 100:
        quantity_lives_str = "30 - 99"
    else:
        quantity_lives_str = "100+"

    query = (
        session.query(Products, HealthInsurances.name)
        .join(HealthInsurances)
        .filter(Products.quantity_lives == quantity_lives_str)
        .filter(Products.health_insurance_id.in_(payload.get("health_insurance", [])))
        .filter(Products.plan == payload.get("plan"))
    )

    if payload.get("compulsory") in (True, False):
        query = query.filter(Products.compulsory == payload.get("compulsory"))

    if payload.get("coparticipation") in (True, False):
        query = query.filter(Products.coparticipation == payload.get("coparticipation"))

    if payload.get("coverage"):
        query = query.filter(Products.coverage == payload.get("coverage"))

    if payload.get("accommodation"):
        query = query.filter(Products.accommodation == payload.get("accommodation"))

    if payload.get("state"):
        query = query.filter(Products.state == payload.get("state"))

    if payload.get("city"):
        query = query.filter(Products.city == payload.get("city"))

    if payload.get("cnpj_type"):
        query = query.filter(Products.cnpj_type == payload.get("cnpj_type"))

    query = query.all()

    if not query:
        return []

    quantity_keys = {
        "age_0_18holder": "age_0_18",
        "age_0_18dependent": "age_0_18",
        "age_19_23holder": "age_19_23",
        "age_19_23dependent": "age_19_23",
        "age_24_28holder": "age_24_28",
        "age_24_28dependent": "age_24_28",
        "age_29_33holder": "age_29_33",
        "age_29_33dependent": "age_29_33",
        "age_34_38holder": "age_34_38",
        "age_34_38dependent": "age_34_38",
        "age_39_43holder": "age_39_43",
        "age_39_43dependent": "age_39_43",
        "age_44_48holder": "age_44_48",
        "age_44_48dependent": "age_44_48",
        "age_49_53holder": "age_49_53",
        "age_49_53dependent": "age_49_53",
        "age_54_58holder": "age_54_58",
        "age_54_58dependent": "age_54_58",
        "age_59_moreholder": "age_59_more",
        "age_59_moredependent": "age_59_more",
    }

    result = {k: v for k, v in payload.items() if k in list(quantity_keys.keys())}

    final_dict = {}

    for k, v in result.items():
        final_dict[quantity_keys[k]] = final_dict.get(quantity_keys[k], 0) + int(v)

    data_frame = consolidate_info(query, final_dict)

    for row in data_frame:
        row["coparticipation"] = "Sim" if row.get("coparticipation") else "Não"
        row["compulsory"] = "Sim" if row.get("compulsory") else "Não"
        row["spg"] = f"{row.pop('quantity_lives')} vidas"
        row.pop("state")
        row.pop("city")
        row.pop("ans")
        row.pop("plan")
        row.pop("cnpj_type")

    return data_frame


@provide_session
def post_request_bradesco(payload, session=None):

    quantity_keys = [
        "age_0_18holder",
        "age_0_18dependent",
        "age_19_23holder",
        "age_19_23dependent",
        "age_24_28holder",
        "age_24_28dependent",
        "age_29_33holder",
        "age_29_33dependent",
        "age_34_38holder",
        "age_34_38dependent",
        "age_39_43holder",
        "age_39_43dependent",
        "age_44_48holder",
        "age_44_48dependent",
        "age_49_53holder",
        "age_49_53dependent",
        "age_54_58holder",
        "age_54_58dependent",
        "age_59_moreholder",
        "age_59_moredependent",
    ]

    quantity_keys_holders = [
        "age_0_18holder",
        "age_19_23holder",
        "age_24_28holder",
        "age_29_33holder",
        "age_34_38holder",
        "age_39_43holder",
        "age_44_48holder",
        "age_49_53holder",
        "age_54_58holder",
        "age_59_moreholder",
    ]

    quantity_lives = sum([int(v) for k, v in payload.items() if k in quantity_keys])
    quantity_holders = sum(
        [int(v) for k, v in payload.items() if k in quantity_keys_holders]
    )
    if quantity_holders < 1:
        return []
    elif quantity_holders == 1 and quantity_lives <= 3:
        quantity_lives_str = "2 - 29"
    else:
        quantity_lives_str = "30 - 99"

    query = (
        session.query(Products, HealthInsurances.name)
        .join(HealthInsurances)
        .filter(Products.quantity_lives == quantity_lives_str)
        .filter(Products.health_insurance_id == BRADESCO_ID)
        .filter(Products.plan == payload.get("plan"))
    )

    if payload.get("compulsory") in (True, False):
        query = query.filter(Products.compulsory == payload.get("compulsory"))

    if payload.get("coparticipation") in (True, False):
        query = query.filter(Products.coparticipation == payload.get("coparticipation"))

    if payload.get("coverage"):
        query = query.filter(Products.coverage == payload.get("coverage"))

    if payload.get("accommodation"):
        query = query.filter(Products.accommodation == payload.get("accommodation"))

    if payload.get("state"):
        query = query.filter(Products.state == payload.get("state"))

    if payload.get("city"):
        query = query.filter(Products.city == payload.get("city"))
    else:
        query = query.filter(Products.city.is_(None))

    if payload.get("cnpj_type"):
        query = query.filter(Products.cnpj_type == payload.get("cnpj_type"))

    query = query.all()

    if not query:
        return []

    quantity_keys = {
        "age_0_18holder": "age_0_18",
        "age_0_18dependent": "age_0_18",
        "age_19_23holder": "age_19_23",
        "age_19_23dependent": "age_19_23",
        "age_24_28holder": "age_24_28",
        "age_24_28dependent": "age_24_28",
        "age_29_33holder": "age_29_33",
        "age_29_33dependent": "age_29_33",
        "age_34_38holder": "age_34_38",
        "age_34_38dependent": "age_34_38",
        "age_39_43holder": "age_39_43",
        "age_39_43dependent": "age_39_43",
        "age_44_48holder": "age_44_48",
        "age_44_48dependent": "age_44_48",
        "age_49_53holder": "age_49_53",
        "age_49_53dependent": "age_49_53",
        "age_54_58holder": "age_54_58",
        "age_54_58dependent": "age_54_58",
        "age_59_moreholder": "age_59_more",
        "age_59_moredependent": "age_59_more",
    }

    result = {k: v for k, v in payload.items() if k in list(quantity_keys.keys())}

    final_dict = {}

    for k, v in result.items():
        final_dict[quantity_keys[k]] = final_dict.get(quantity_keys[k], 0) + int(v)

    data_frame = consolidate_info(query, final_dict)
    for row in data_frame:
        row["coparticipation"] = "Sim" if row.get("coparticipation") else "Não"
        row["compulsory"] = "Sim" if row.get("compulsory") else "Não"
        row.pop("state")
        row.pop("city")
        row.pop("ans")
        row.pop("plan")
        row.pop("cnpj_type")
        q = row.pop("quantity_lives")
        if q == "2 - 29":
            row["spg"] = "Tabela 1"
        else:
            row["spg"] = "Tabela 2"
        row["hospitals"] = ["hospital a", "hospital b", "hospital c"]
        row["laboratories"] = ["laboratorio a", "laboratorio b", "laboratorio c"]

    return data_frame


@provide_session
def get_item_by_id(id, session=None):
    items = []
    for item in session.query(Order).filter_by(id=id).all():
        new_item = item.__dict__
        del new_item["_sa_instance_state"]
        items.append(new_item)

    return items


@provide_session
def get_all(session=None):
    items = []
    for item in session.query(Order).all():
        new_item = item.__dict__
        del new_item["_sa_instance_state"]
        items.append(new_item)

    return items


@provide_session
def calculate_sheets_information(file: bytes, session=None):
    with NamedTemporaryFile("wb") as f:
        f.write(file)
        f.flush()
        sheets_header = pandas.read_excel(
            f.name, usecols="A:B", nrows=7, index_col=None, header=None
        )
        sheets_header = pandas.DataFrame(sheets_header.fillna(value=False))
        plan = sheets_header[1][0]
        state = sheets_header[1][1]
        city = sheets_header[1][2]
        coverage = sheets_header[1][3]
        coparticipation = sheets_header[1][4]
        compulsory = sheets_header[1][5]
        cnpj_type = sheets_header[1][6]
        payload = {
            "plan": plan,
            "state": state,
            "city": city,
            "coverage": coverage,
            "coparticipation": coparticipation,
            "compulsory": compulsory,
            "cnpj_type": cnpj_type,
        }
        df = pandas.read_excel(f.name, usecols="A:E", skiprows=7)
    df = pandas.DataFrame(df.fillna(value=False))

    for k in payload:
        if k != "city":
            if (payload[k]) is None:
                return {}

    if payload.get("city") is False:
        payload["city"] = None

    plans = {"Empresarial": 1, "Odontológico": 2, "Produto Adesão": 3}

    for k, v in plans.items():
        if k == payload.get("plan"):
            payload["plan"] = v

    booleanable = ["compulsory", "coparticipation"]
    for key, value in payload.copy().items():
        if key in booleanable:
            if value == "Sim":
                payload[key] = True
            elif value == "Não":
                payload[key] = False
            else:
                del payload[key]

    if payload.get("coverage") == "Indiferente":
        del payload["coverage"]
    else:
        payload["coverage"] = payload.get("coverage").lower()

    if payload["cnpj_type"] == "Nenhum":
        del payload["cnpj_type"]

    ages = {
        "age_0_18": (0, 18),
        "age_19_23": (19, 23),
        "age_24_28": (24, 28),
        "age_29_33": (29, 33),
        "age_34_38": (34, 38),
        "age_39_43": (39, 43),
        "age_44_48": (44, 48),
        "age_49_53": (49, 53),
        "age_54_58": (54, 58),
        "age_59_more": (59, 1000),
    }

    df = df.groupby(["Dependente", "Idade"]).agg({"Idade": "count"}).to_dict()
    idades = df.get("Idade")
    data = {}
    for valores, k in idades.items():
        dependente = False
        if valores[0] is not False:
            dependente = True
        for w, idades in ages.items():
            # return str(valores)
            if valores[1] >= idades[0] and valores[1] <= idades[1]:
                if dependente:
                    data[f"{w}dependent"] = data.get(f"{w}dependent", 0) + k
                else:
                    data[f"{w}holder"] = data.get(f"{w}holder", 0) + k
                continue

    payload = {**payload, **data}
    health_insurance_ids = session.query(HealthInsurances.id).all()

    if not health_insurance_ids:
        return []

    hi_ids = []
    for id in health_insurance_ids:
        hi_ids.append(id[0])

    if BRADESCO_ID in hi_ids:
        hi_ids.remove(BRADESCO_ID)
    payload["health_insurance"] = hi_ids
    return post_request_bradesco(payload) + post_request(payload)
