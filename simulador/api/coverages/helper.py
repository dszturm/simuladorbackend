from simulador.utils.db import provide_session
from simulador.models.coverages import Coverage


# @provide_session
# def process_request(payload, session=None):
#     pass
# payload = {"name": "Bradesco"}

# product = Coverage(**payload)
# session.add(product)
# session.commit()

# return {"id": product.id}


@provide_session
def get_all(session=None):
    items = []
    for item in session.query(Coverage).all():
        new_item = item.__dict__
        del new_item["_sa_instance_state"]
        items.append(new_item)

    return items
