from simulador.utils.db import provide_session
from simulador.models.accommodations import Accommodation


# @provide_session
# def process_request(payload, session=None):
#     pass
# payload = {"name": "Bradesco"}

# product = Coverage(**payload)
# session.add(product)
# session.commit()

# return {"id": product.id}


@provide_session
def get_all(session=None):
    items = []
    for item in session.query(Accommodation).all():
        new_item = item.__dict__
        if new_item.get("name"):
            items.append(new_item.get("name"))

    return items
