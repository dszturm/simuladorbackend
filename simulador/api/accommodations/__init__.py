from flask_restplus import Resource, reqparse
from simulador.api import api
from .helper import get_all

ns = api.namespace("v1/accommodation", description="Accommodations")

# parser_auth = reqparse.RequestParser()
# parser_auth.add_argument("Authorization", location="headers", required=True)
# parser_auth.add_argument("teste", type=int, required=False)
# parser_auth.add_argument("teste2", action="split") # splita uma string pela virgula, semelhante a str.split(",")

parser_info = reqparse.RequestParser()
parser_info.add_argument("id", type=int, required=True)

# # todo: where to store data about simulations?
coverage_body = {"type": "object", "properties": {"name": {"type": "string"}}}


coverage_schema = api.schema_model("POST Accommodation V1", coverage_body)


@ns.route("")
class ProductRoute(Resource):
    # @ns.expect(None, coverage_schema, validate=True)
    # @api.response(200, "Success")
    # def post(self):
    #     response = process_request(dict(request.json))

    #     # response = {"test": "testado"}
    #     return response, 200

    @api.response(200, "Success")
    def get(self):
        return get_all()
