from flask_restplus import Resource
from flask import request
from simulador.api import api
from .helper import process_request

ns = api.namespace("v1/simulate", description="Simulate health plan")

# parser_auth = reqparse.RequestParser()
# parser_auth.add_argument("Authorization", location="headers", required=True)
# parser_auth.add_argument("teste", type=int, required=False)
# parser_auth.add_argument("teste2", action="split") # splita uma string pela virgula, semelhante a str.split(",")

# # todo: where to store data about simulations?

simulate_body = {
    "type:": "object",
    "required": [
        "company_name",
        "company_contact",
        "phone",
        "state",
        "coverage",
        "co_participation",
        "optional_or_compulsory",
        "health_companies",
        "accommodation",
    ],
    "properties": {
        "company_name": {"type": "string"},
        "company_contact": {"type": "string"},
        "phone": {"type": "string"},
        "state": {"type": "string"},
        "coverage": {"type": "string"},
        "co_participation": {"type": "string"},
        "optional_or_compulsory": {"type": "string"},
        "health_companies": {"type": "array", "items": {"type": "string"}},
        "accommodation": {"type": "string"},
        "age_perfil": {
            "type": "object",
            "properties": {
                "age": {
                    "type": "object",
                    "properties": {
                        "holder": {"type": "integer"},
                        "dependant": {"type": "integer"},
                    },
                }
            },
        },
    },
}


post_simulate = api.schema_model("POST Simulate V1", simulate_body)


# transaction_model_template = {
#     "id": fields.Integer,
#     "acquirer_id": fields.Integer,
#     "acquirer": fields.String,
#     "merchant_id": fields.Integer,
#     "merchant": fields.String,
#     "terminal_id": fields.Integer,
#     "terminal": fields.String,
#     "brand_id": fields.Integer,
#     "brand": fields.String,
#     "transaction_type_id": fields.Integer,
#     "transaction_type": fields.String,
#     "gross_amount": fields.Float,
#     "net_amount": fields.Float,
#     "gross_amount_first_installment": fields.Float,
#     "acquirer_fee_percentual": fields.Float,
#     "acquirer_fee_total": fields.Float,
#     "installments_total": fields.Integer,
#     "nsu": fields.String,
#     "authorization_code": fields.String,
#     "batch_number": fields.String,
#     "card_number": fields.String,
#     "transaction_date": fields.Date,
#     "transaction_hour": fields.String,
#     "transaction_timestamp": fields.DateTime,
#     "gross_amount_next_installments": fields.Float,
#     "customer_id": fields.String,
#     "payment_status_id": fields.Integer,
#     "payment_status": fields.String,
#     "conciliation_status_id": fields.Integer,
#     "conciliation_status": fields.String,
# }

# transaction_model = api.model("Transactions V1", transaction_model_template)


@ns.route("")
class SimulateRoute(Resource):
    def post(self):
        response = process_request(dict(request.json))

        return response, 200
