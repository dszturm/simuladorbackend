from flask_restplus import Resource, reqparse
from flask import request
from simulador.api import api
from .helper import process_request, get_item_by_id, get_all, delete_item_by_id

ns = api.namespace("v1/health-insurance", description="Health insurance")

parser_info = reqparse.RequestParser()
parser_info.add_argument("id", type=int, required=True)

health_insurance_body = {
    "type:": "object",
    "required": ["name"],
    "properties": {"name": {"type": "string"}, "minLength": 1},
}


post_health_insurance = api.schema_model(
    "POST Health insurance V1", health_insurance_body
)


@ns.route("")
class HealthInsuranceRoute(Resource):
    @ns.expect(None, post_health_insurance, validate=True)
    @api.response(200, "Success")
    def post(self):
        health_insurance_copy = dict(request.json).copy()
        health_insurance_item = {}
        health_insurance_item["name"] = health_insurance_copy.get("name")
        response = process_request(health_insurance_item)

        if response.get("error"):
            return response, 400

        return response, 200

    @api.response(200, "Success")
    def get(self):
        return get_all()


@ns.route("/<id>")
@ns.param("id", "Product's identification number")
class HealthInsuranceIdRoute(Resource):
    @ns.expect(parser_info, validate=True)
    @api.response(200, "Success", post_health_insurance)
    def get(self, id):
        return get_item_by_id(id)

    @ns.expect(None, validate=True)
    @api.response(200, "Success")
    def delete(self, id):
        return delete_item_by_id(int(id))
