from simulador.utils.db import provide_session
from simulador.models.health_insurances import HealthInsurances
from sqlalchemy.exc import IntegrityError


@provide_session
def process_request(payload, session=None):
    name = payload.get("name", "")

    if name.strip() == "":
        return {"error": "Nome de operadora NÃO informado."}

    if session.query(HealthInsurances).filter(HealthInsurances.name == name).all():
        return {"error": "Nome de operadora JÁ registrado."}

    health_company = HealthInsurances(**{"name": name})
    session.add(health_company)
    session.commit()

    return {"id": health_company.id}


@provide_session
def get_all(session=None):
    items = []
    for item in session.query(HealthInsurances).all():
        new_item = item.__dict__
        del new_item["_sa_instance_state"]
        items.append(new_item)

    return items


@provide_session
def get_item_by_id(id, session=None):
    items = []
    for item in session.query(HealthInsurances).filter_by(id=id).all():
        new_item = item.__dict__
        del new_item["_sa_instance_state"]
        items.append(new_item)

    return items


@provide_session
def delete_item_by_id(id: int, session=None):
    try:
        health_insurance = (
            session.query(HealthInsurances).filter(HealthInsurances.id == id).first()
        )
        if not health_insurance:
            return {"message": "ID da operadora não encontrado."}, 400
        name = health_insurance.name
        session.query(HealthInsurances).filter(HealthInsurances.id == id).delete()
        session.commit()
        return (
            {
                "message": "Operadora de 'id: {}' e 'nome: {}' deletado.".format(
                    id, name
                )
            },
            200,
        )
    except IntegrityError:
        return {
            "error": "Operadora utilizada em alguns produtos. Operadora não foi deletada."
        }
    except Exception as identifier:
        return {"error": str(identifier.__class__)}, 400
