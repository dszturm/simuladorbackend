from flask_restplus import Resource, fields
from flask import request
from pintassilgo.api import api, parser_get_data, parser_auth
from werkzeug.exceptions import Unauthorized
from pintassilgo.api.auth import AuthHelper
from .helper import get_transactions, get_transaction_by_id

ns = api.namespace("v1/transactions", description="Get Sales")

transaction_model_template = {
    "id": fields.Integer,
    "acquirer_id": fields.Integer,
    "acquirer": fields.String,
    "merchant_id": fields.Integer,
    "merchant": fields.String,
    "terminal_id": fields.Integer,
    "terminal": fields.String,
    "brand_id": fields.Integer,
    "brand": fields.String,
    "transaction_type_id": fields.Integer,
    "transaction_type": fields.String,
    "gross_amount": fields.Float,
    "net_amount": fields.Float,
    "gross_amount_first_installment": fields.Float,
    "acquirer_fee_percentual": fields.Float,
    "acquirer_fee_total": fields.Float,
    "installments_total": fields.Integer,
    "nsu": fields.String,
    "authorization_code": fields.String,
    "batch_number": fields.String,
    "card_number": fields.String,
    "transaction_date": fields.Date,
    "transaction_hour": fields.String,
    "transaction_timestamp": fields.DateTime,
    "gross_amount_next_installments": fields.Float,
    "customer_id": fields.String,
    "payment_status_id": fields.Integer,
    "payment_status": fields.String,
    "conciliation_status_id": fields.Integer,
    "conciliation_status": fields.String,
}

transaction_model = api.model("Transactions V1", transaction_model_template)


@ns.route("")
class TransactionsRoute(Resource):
    @ns.expect(parser_get_data, validate=False)
    @api.response(200, "Success", [transaction_model])
    def get(self):
        args = parser_get_data.parse_args(request)
        is_valid = AuthHelper.check_token({"Authorization": args.get("Authorization")})
        if not is_valid:
            raise Unauthorized("User not authorized")

        transactions = get_transactions(
            customer_id=args.get("customer_id"),
            start_date=args.get("start_date"),
            end_date=args.get("end_date"),
            path=request.path,
            version=1,
        )

        return transactions, 200


@ns.route("/<id>")
@ns.param("id", "Transaction id")
class TransactionIdRoute(Resource):
    @ns.expect(parser_auth, validate=False)
    @api.response(200, "Success", transaction_model)
    def get(self, id):
        args = parser_auth.parse_args(request)
        is_valid = AuthHelper.check_token({"Authorization": args.get("Authorization")})
        if not is_valid:
            raise Unauthorized("User not authorized")

        transaction = get_transaction_by_id(transaction_id=id, version=1,)

        return transaction, 200
