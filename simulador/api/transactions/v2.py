from flask_restplus import Resource, fields, reqparse
from flask import request
from pintassilgo.api import api, parser_get_data, parser_auth, default_post_response
from werkzeug.exceptions import Unauthorized
from pintassilgo.api.auth import AuthHelper
from .helper import (
    get_transactions,
    get_transaction_by_id,
    create_transaction,
    update_transaction,
    delete_transaction,
)
from .v1 import transaction_model_template

parser_auth = reqparse.RequestParser()
parser_auth.add_argument("Authorization", location="headers", required=True)
parser_auth.add_argument("teste", type=str, required=False)

ns = api.namespace("v2/transactions", description="Get Sales")

transaction_model_template_v2 = {
    **transaction_model_template,
    **{
        "data_source_id": fields.Integer,
        "data_source": fields.String,
        "payment_method_id": fields.Integer,
        "payment_method": fields.String,
        "order_id": fields.Integer,
        "bank_account_id": fields.Integer,
        "bank_number": fields.Integer,
        "bank_name": fields.String,
        "branch_number": fields.String,
        "account_number": fields.String,
        "is_revenue": fields.Boolean,
        "tags": fields.String,
        "recurring_transaction_id": fields.Integer,
    },
}

transaction_model = api.model("Transactions V2", transaction_model_template_v2)

body_post_transaction = {
    "type:": "object",
    "required": [
        "alternate_id",
        "acquirer_id",
        "merchant_id",
        "brand_id",
        "transaction_type_id",
        "transaction_date",
        "customer_id",
        "is_revenue",
    ],
    "properties": {
        "alternate_id": {"type": "string"},
        "acquirer_id": {"type": "integer"},
        "merchant_id": {"type": "integer"},
        "brand_id": {"type": "integer"},
        "transaction_type_id": {"type": "integer"},
        "gross_amount": {"type": "number"},
        "net_amount": {"type": "number"},
        "gross_amount_first_installment": {"type": "number"},
        "acquirer_fee_percentual": {"type": "number"},
        "acquirer_fee_total": {"type": "number"},
        "installments_total": {"type": "integer"},
        "nsu": {"type": "string"},
        "authorization_code": {"type": "string"},
        "batch_number": {"type": "string"},
        "card_number": {"type": "string"},
        "transaction_date": {"type": "string", "format": "date"},
        "transaction_hour": {"type": "string"},
        "transaction_timestamp": {"type": "string", "format": "date-time"},
        "gross_amount_next_installments": {"type": "number"},
        "customer_id": {"type": "integer"},
        "payment_status_id": {"type": "integer"},
        "conciliation_status_id": {"type": "integer"},
        "payment_method_id": {"type": "integer"},
        "order_id": {"type": "integer"},
        "description": {"type": "string"},
        "tags": {"type": "array", "items": {"type": "string"}},
        "bank_account_id": {"type": "integer"},
        "is_revenue": {"type": "boolean"},
        "recurring_transaction_id": {"type": "integer"},
    },
}
post_transaction_body = api.schema_model("POST Transaction V2", body_post_transaction)


@ns.route("")
class TransactionsRoute(Resource):
    @ns.expect(parser_get_data, validate=True)
    @api.response(200, "Success", [transaction_model])
    def get(self):
        args = parser_get_data.parse_args(request)
        is_valid = AuthHelper.check_token({"Authorization": args.get("Authorization")})
        if not is_valid:
            raise Unauthorized("User not authorized")

        transactions = get_transactions(
            customer_id=args.get("customer_id"),
            start_date=args.get("start_date"),
            end_date=args.get("end_date"),
            path=request.path,
            version=2,
        )

        return transactions, 200

    @ns.expect(parser_auth, post_transaction_body, validate=True)
    @api.response(200, "Success", default_post_response)
    def post(self):
        args = parser_auth.parse_args(request)
        is_valid = AuthHelper.check_token({"Authorization": args.get("Authorization")})
        if not is_valid:
            raise Unauthorized("User not authorized")

        transaction = create_transaction(dict(request.json))

        return transaction, 200


@ns.route("/<id>")
@ns.param("id", "Transaction id")
class TransactionIdRoute(Resource):
    @ns.expect(parser_auth, validate=False)
    @api.response(200, "Success", transaction_model)
    def get(self, id):
        args = parser_auth.parse_args(request)
        is_valid = AuthHelper.check_token({"Authorization": args.get("Authorization")})
        if not is_valid:
            raise Unauthorized("User not authorized")

        transaction = get_transaction_by_id(transaction_id=id, version=2,)

        return transaction, 200

    @ns.expect(parser_auth, body_post_transaction, validate=True)
    @api.response(200, "Success", default_post_response)
    def put(self, id):
        args = parser_auth.parse_args(request)
        is_valid = AuthHelper.check_token({"Authorization": args.get("Authorization")})
        if not is_valid:
            raise Unauthorized("User not authorized")

        transaction = update_transaction(id, dict(request.json))

        return transaction, 200

    @ns.expect(parser_auth, validate=True)
    @api.response(200, "Success")
    def delete(self, id):
        args = parser_auth.parse_args(request)
        is_valid = AuthHelper.check_token({"Authorization": args.get("Authorization")})
        if not is_valid:
            raise Unauthorized("User not authorized")

        response = delete_transaction(id)

        return response, 200
