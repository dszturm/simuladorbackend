from pintassilgo.models.event_transaction import EventTransaction
from pintassilgo.utils.db import (
    provide_read_session,
    provide_write_session,
    check_if_read_replica_ready,
)
from pintassilgo.models.domain_acquirer import DomainAcquirer
from pintassilgo.models.domain_transaction_type import DomainTransactionType
from pintassilgo.models.domain_brand import DomainBrand
from pintassilgo.models.domain_merchant import DomainMerchant
from pintassilgo.models.domain_terminal import DomainTerminal
from pintassilgo.utils.cache_controller import create_cache, verify_dates
from sqlalchemy.sql.expression import literal_column
from datetime import datetime
from sqlalchemy import Text, Integer
from pintassilgo.utils.log_handler import catch_errors
from sqlalchemy.sql.expression import false
from pintassilgo.models.domain_transaction_match_type import DomainTransactionMatchType
from pintassilgo.models.domain_data_source import DomainDataSource
from pintassilgo.models.domain_payment_method import DomainPaymentMethod
from werkzeug.exceptions import BadRequest
from sqlalchemy.exc import IntegrityError
from pintassilgo.models.domain_bank import DomainBank
from pintassilgo.models.domain_bank_account import DomainBankAccount


@provide_read_session
def _get_transaction_query(version, session=None):
    transaction_query = (
        session.query(
            EventTransaction.id.label("id"),
            EventTransaction.acquirer_id.label("acquirer_id"),
            DomainAcquirer.name.label("acquirer"),
            EventTransaction.merchant_id.label("merchant_id"),
            DomainMerchant.name.label("merchant"),
            EventTransaction.terminal_id.label("terminal_id"),
            DomainTerminal.terminal_code.label("terminal"),
            EventTransaction.brand_id.label("brand_id"),
            DomainBrand.name.label("brand"),
            EventTransaction.transaction_type_id.label("transaction_type_id"),
            DomainTransactionType.name.label("transaction_type"),
            EventTransaction.gross_amount.label("gross_amount"),
            EventTransaction.net_amount.label("net_amount"),
            EventTransaction.gross_amount_first_installment.label(
                "gross_amount_first_installment"
            ),
            EventTransaction.acquirer_fee_percentual.label("acquirer_fee_percentual"),
            EventTransaction.acquirer_fee_total.label("acquirer_fee_total"),
            EventTransaction.installments_total.label("installments_total"),
            EventTransaction.nsu.label("nsu"),
            EventTransaction.authorization_code.label("authorization_code"),
            EventTransaction.batch_number.label("batch_number"),
            EventTransaction.card_number.label("card_number"),
            EventTransaction.transaction_date.label("transaction_date"),
            EventTransaction.transaction_hour.label("transaction_hour"),
            EventTransaction.transaction_timestamp.label("transaction_timestamp"),
            EventTransaction.gross_amount_next_installments.label(
                "gross_amount_next_installments"
            ),
            EventTransaction.customer_id.label("customer_id"),
            literal_column("0", type_=Integer).label("payment_status_id"),
            literal_column("'tbd'", type_=Text).label("payment_status"),
            EventTransaction.transaction_match_type_id.label("conciliation_status_id"),
            DomainTransactionMatchType.name.label("conciliation_status"),
        )
        .outerjoin(DomainAcquirer)
        .outerjoin(DomainBrand)
        .outerjoin(DomainTransactionType)
        .outerjoin(DomainMerchant)
        .outerjoin(DomainTerminal)
        .outerjoin(DomainTransactionMatchType)
    )
    if version == 2:
        transaction_query = (
            transaction_query.outerjoin(DomainDataSource)
            .outerjoin(DomainPaymentMethod)
            .outerjoin(
                DomainBankAccount,
                EventTransaction.bank_account_id == DomainBankAccount.id,
            )
            .outerjoin(DomainBank, DomainBankAccount.bank_id == DomainBank.id)
            .add_columns(
                EventTransaction.data_source_id.label("data_source_id"),
                DomainDataSource.name.label("data_source"),
                EventTransaction.payment_method_id.label("payment_method_id"),
                DomainPaymentMethod.name.label("payment_method"),
                EventTransaction.order_id.label("order_id"),
                EventTransaction.tags.label("tags"),
                EventTransaction.description.label("description"),
                EventTransaction.bank_account_id.label("bank_account_id"),
                DomainBank.alternate_id.label("bank_number"),
                DomainBank.name.label("bank_name"),
                DomainBankAccount.branch_number.label("branch_number"),
                DomainBankAccount.account_number.label("account_number"),
                EventTransaction.is_revenue.label("is_revenue"),
                EventTransaction.recurring_transaction_id.label(
                    "recurring_transaction_id"
                ),
            )
        )

    transaction_query = transaction_query.filter(
        EventTransaction.is_deleted == false()
    ).order_by(EventTransaction.transaction_date.asc())
    return transaction_query


@catch_errors
def get_transaction_by_id(transaction_id, version):
    transaction_response = (
        _get_transaction_query(version=version)
        .filter(EventTransaction.id == transaction_id)
        .first()
    )
    if not transaction_response:
        return {}
    return transaction_response._asdict()


@catch_errors
@provide_read_session
def get_transactions(path, customer_id, start_date, end_date, version, session=None):
    transaction_response = []
    cache_ids = []
    new_cache_id = []

    start_date = datetime.strptime(start_date, "%Y-%m-%d")
    end_date = datetime.strptime(end_date, "%Y-%m-%d")

    new_start_date, new_end_date, cache_ids = verify_dates(
        start_date, end_date, customer_id, path
    )

    if new_start_date and new_end_date:
        transaction_response = (
            _get_transaction_query(version=version)
            .filter(EventTransaction.transaction_date >= new_start_date)
            .filter(EventTransaction.transaction_date <= new_end_date)
            .filter(EventTransaction.customer_id == customer_id)
            .all()
        )

        if transaction_response:
            new_cache_id = [
                create_cache(
                    new_start_date,
                    new_end_date,
                    customer_id,
                    path,
                    [x._asdict() for x in transaction_response],
                )
            ]

    return {"cache_ids": list(set([*cache_ids, *new_cache_id]))}


@catch_errors
@provide_write_session
def create_transaction(payload, session=None):
    payload["transaction_match_type_id"] = payload.pop("conciliation_status_id")

    try:
        transaction = EventTransaction(**payload)
        session.add(transaction)
        session.commit()
    except IntegrityError as e:
        raise BadRequest(f"Database explicity denied your request with message: {e}")

    check_if_read_replica_ready(
        table=EventTransaction,
        table_pk_value=transaction.id,
        condition_name="id",
        condition_value=transaction.id,
        timeout=20,
    )

    return {"id": transaction.id}


@catch_errors
@provide_write_session
def update_transaction(id, payload, session=None):
    if session.query(EventTransaction.id).filter(EventTransaction.id == id).all():
        payload["last_updated"] = datetime.now()
        payload["transaction_match_type_id"] = payload.pop("conciliation_status_id")

        try:
            session.query(EventTransaction).filter(EventTransaction.id == id).update(
                {getattr(EventTransaction, k): v for k, v in payload.items()}
            )
            session.commit()
        except IntegrityError as e:
            raise BadRequest(
                f"Database explicity denied your request with message: {e}"
            )

        check_if_read_replica_ready(
            table=EventTransaction,
            table_pk_value=id,
            condition_name="last_updated",
            condition_value=payload["last_updated"],
            timeout=20,
        )

    return {"status": "ok"}


@catch_errors
@provide_write_session
def delete_transaction(id, session=None):
    if session.query(EventTransaction.id).filter(EventTransaction.id == id).all():
        try:
            session.query(EventTransaction).filter(EventTransaction.id == id).update(
                {
                    EventTransaction.is_deleted: True,
                    EventTransaction.last_updated: datetime.now(),
                }
            )
            session.commit()
        except IntegrityError as e:
            raise BadRequest(
                f"Database explicity denied your request with message: {e}"
            )

        check_if_read_replica_ready(
            table=EventTransaction,
            table_pk_value=id,
            condition_name="is_deleted",
            condition_value="True",
            timeout=20,
        )

    return {"status": "ok"}
