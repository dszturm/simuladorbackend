from flask_restplus import Resource
from simulador.api import api
from .helper import get_all

ns = api.namespace("v1/location", description="Location")


@ns.route("")
class LocationRoute(Resource):
    @api.response(200, "Success")
    def get(self):
        return get_all()
