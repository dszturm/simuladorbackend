from simulador.utils.db import provide_session
from simulador.models.products import Products


@provide_session
def get_all(session=None):
    items = []
    for item in session.query(Products.state, Products.city).distinct().all():
        items.append(item._asdict())

    locations = {}
    for item in items:
        state_is_in = False
        for state in locations:
            if item["state"] == state:
                state_is_in = True
                pass
        if state_is_in:
            cities = locations.get(item.get("state"))
            cities.append(item.get("city"))
            locations = {**locations, item.get("state"): cities}
        else:
            locations = {**locations, item.get("state"): [item.get("city")]}

    return locations
