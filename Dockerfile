FROM 503924640744.dkr.ecr.us-east-1.amazonaws.com/python-pandas:latest

RUN apk add --no-cache \
            xvfb \
            # Additionnal dependencies for better rendering
            ttf-freefont \
            fontconfig \
            dbus \
    && \
    # Install wkhtmltopdf from `testing` repository
    apk add qt5-qtbase-dev \
            wkhtmltopdf \
            --no-cache \
            --repository http://dl-3.alpinelinux.org/alpine/edge/testing/ \
            --allow-untrusted \
    && \
    # Wrapper for xvfb
    mv /usr/bin/wkhtmltopdf /usr/bin/wkhtmltopdf-origin && \
    echo $'#!/usr/bin/env sh\n\
Xvfb :0 -screen 0 1024x768x24 -ac +extension GLX +render -noreset & \n\
DISPLAY=:0.0 wkhtmltopdf-origin $@ \n\
killall Xvfb\
' > /usr/bin/wkhtmltopdf && \
    chmod +x /usr/bin/wkhtmltopdf

RUN apk add --no-cache --update python3-dev gcc build-base zlib-dev jpeg-dev python3-dev musl-dev postgresql-dev wkhtmltopdf msttcorefonts-installer ttf-liberation  ttf-linux-libertine

WORKDIR /app

ENV PYTHONPATH "/app:${PYTHONPATH}"

COPY requirements.txt /app
RUN pip install -r requirements.txt

COPY . /app

RUN python setup.py develop

EXPOSE 5000

ENTRYPOINT ["sh", "entrypoint.sh"]
